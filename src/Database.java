import java.lang.reflect.Array;
import java.util.*;

public class Database {
    public List<Question> questions;

    public Database(){
        questions = Collections.emptyList();
    }

    public Database(Question any){
        super();
    }

    public  Database(Question cat, Question pv){
        super();
    }


    public List<Question> getQuestions() {
        return questions;
    }

    public String getCategory(Question c){
        return c.category;
    }

    public Integer getPointValue(Question pv){
       return pv.pointValue;
    }

    public String selectQuestion(Question recPV, Question recCAT) {
        int pointVal = getPointValue(recPV);
        String category = getCategory(recCAT);
        String question = "";
        List<Question> db = setQuestions();

        Iterator itr = db.iterator();

        Question comparableQ = new Question("",pointVal,category, "");


            while(itr.hasNext()) {
                Question otherQ = (Question) itr.next();
                if (otherQ.pointValue == comparableQ.pointValue && otherQ.category == comparableQ.category) {
                    question = otherQ.question;
                }
            }

        return question;
    }



    public String getAnswer(Question recPV, Question recCAT) {
        int pointVal = getPointValue(recPV);
        String category = getCategory(recCAT);
        String answr = "";
        List<Question> db = setQuestions();

        Iterator itr = db.iterator();

        Question comparableQ = new Question("",pointVal,category, "");


        while(itr.hasNext()) {
            Question otherQ = (Question) itr.next();
            if (otherQ.pointValue == comparableQ.pointValue && otherQ.category == comparableQ.category) {
                answr = otherQ.answer;
            }
        }

        return answr;
    }

    public List<Question> setQuestions() {

        /***
         * add question objects below in order to populate the arrayList of object Question by using db.add(<questionName>)
         */
        Question Grammar200 = new Question("What symbols are used to show that someone is speaking?", 200, "Grammar", "quotation marks");
        Question Grammar400 = new Question("What type of clause cannot be a sentence on its own?", 400, "Grammar", "dependent");
        Question Grammar600 = new Question ("There are 3 types of sentence structures: simple, compound and what?", 600, "Grammar", "complex");
        Question Grammar800 = new Question ("Of, in, and on are examples of what type of speech", 800, "Grammar", "preposition");
        Question Grammar1000 = new Question ("A verb ending in -ing that is the subject of a sentenced is called what?", 1000, "Grammar", "gerund");

        Question AmericanHistory200 = new Question("What month and day does the U.S. celebrate its independence?", 200, "American History", "july 4");
        Question AmericanHistory400 = new Question("Who wrote the Declaration of Independence?", 400, "American History", "john hancock");
        Question AmericanHistory600 = new Question ("During Paul Revere's Ride, who else did he ride with besides Samuel Prescott?", 600, "American History", "william dawes");
        Question AmericanHistory800 = new Question ("In what city was the Declaration of Independence signed?", 800, "American History", "philadelphia");
        Question AmericanHistory1000 = new Question ("A tax on this started a revolution in Boston.", 1000, "American History", "tea");

        Question Anatomy200 = new Question("There are 33 of these in the human spine.", 200, "Anatomy", "vertabrae");
        Question Anatomy400 = new Question("What is the largest muscle in the human body?", 400, "Anatomy", "gluteus maximus");
        Question Anatomy600 = new Question ("What is the largest bone in the human body?", 600, "Anatomy", "femur");
        Question Anatomy800 = new Question ("What group of muscles allows someone to rotate their shoulder?", 800, "Anatomy", "rotator cuff");
        Question Anatomy1000 = new Question ("What bone does does the radius rotate around?", 1000, "Anatomy", "ulna");

        Question Animals200 = new Question("What is the name of a group of lions?", 200, "Animals", "pride");
        Question Animals400 = new Question("What is the largest bird?", 400, "Animals", "ostrich");
        Question Animals600 = new Question ("How many legs do lobsters have?", 600, "Animals", "10");
        Question Animals800 = new Question ("What is the largest lizard?", 800, "Animals", "komodo dragon");
        Question Animals1000 = new Question ("A murder is a group of what?", 1000, "Animals", "crows");

        Question Geography200 = new Question("What is the smallest state in the United States of America?", 200, "Geography", "rhode island");
        Question Geography400 = new Question("What is the largest state in the United States of America?", 400, "Geography", "alaska");
        Question Geography600 = new Question ("What is the least populated continent?", 600, "Geography", "antarctica");
        Question Geography800 = new Question ("What country is Machu Picchu located in?", 800, "Geography", "peru");
        Question Geography1000 = new Question ("Which country has the most lakes?", 1000, "Geography", "canada");

        Question Literature200 = new Question("Who wrote 1984?", 200, "Literature", "george orwell");
        Question Literature400 = new Question("The letter A in A Scarlet Letter stood for this?", 400, "Literature", "adulterer");
        Question Literature600 = new Question ("In this book, Jonas is the receiver of dreams?", 600, "Literature", "the giver");
        Question Literature800 = new Question ("What book is about young boys stranded on a deserted island?", 800, "Literature", "lord of the flies");
        Question Literature1000 = new Question ("In literature, books about an ideal society are called this?", 1000, "Literature", "utopia");

        Question Presidents400 = new Question("Who is the first president of the United States?", 400, "Presidents", "george washington");
        Question Presidents800 = new Question("Which president orchestrated the Louisiana Purchase?", 800, "Presidents", "thomas jefferson");
        Question Presidents1200 = new Question ("Who is the fourth president of the United States?", 1200, "Presidents", "james madison");
        Question Presidents1600 = new Question ("Which president served two non-consecutive terms?", 1600, "Presidents", "grover cleveland");
        Question Presidents2000 = new Question ("Which president preceded Abraham Lincoln?", 2000, "Presidents", "james buchanan");

        Question Government400 = new Question("Executive, Judicial, and this are the three branches of government.", 400, "Government", "legislative");
        Question Government800 = new Question("How many justices are on the Supreme Court?", 800, "Government", "9");
        Question Government1200 = new Question ("How many electoral college votes are there in total?", 1200, "Government", "537");
        Question Government1600 = new Question ("Which state has the most electoral college votes?",1600,"Government","california");
        Question Government2000 = new Question ("Who was the first Chief Justice of the United States?", 2000, "Government", "john jay");

        Question Chemistry400 = new Question("What is the chemical formula for water", 400, "Chemistry", "h2o");
        Question Chemistry800 = new Question("Helium, Neo, and Argon are considered this type of gases.", 800, "Chemistry","noble");
        Question Chemistry1200 = new Question ("What is the periodic symbol for lead?", 1200, "Chemistry", "pb");
        Question Chemistry1600 = new Question ("How many elements are on the periodic table?", 1600, "Chemistry", "118");
        Question Chemistry2000 = new Question ("This type of metal makes up the most elements on the periodic table.", 2000, "Chemistry", "transition");


        Question Physics400 = new Question("What is the force that opposes the  motion of two bodies that are in contact?", 400, "Physics", "friction");
        Question Physics800 = new Question("This temperature scale is named after a Swedish scientist.", 800, "Physics", "celsius");
        Question Physics1200 = new Question ("What is the ability of fluids to resistant flow?", 1200, "Physics", "viscosity");
        Question Physics1600 = new Question ("Henry is a unit used to measure this.", 1600, "Physics", "inductance");
        Question Physics2000 = new Question ("What is a cylindrical coil of wire where a magnetic field is created when an electric current passes though it?", 2000, "Physics", "solenoid");

        Question Psychology400 = new Question("What is the last name of the psychologist that is famous for his study with dogs?", 400, "Psychology", "pavlov");
        Question Psychology800 = new Question("What is the last name of the person that is considered the father of modern psychology?", 800, "Psychology", "freud");
        Question Psychology1200 = new Question ("Schrodinger's famous thought experiment was about what kind of animal?", 1200, "Psychology", "cat");
        Question Psychology1600 = new Question ("This person's' hierarchy of needs leads one to self-actualization.", 1600, "Psychology", "maslow");
        Question Psychology2000 = new Question ("What is the last name of the psychologist that orchestrated the Stanford Prison Experiment", 2000, "Psychology", "zambardo");

        Question WorldHistory400 = new Question("The British government exported many of its prisoner's to this island country.", 400, "WorldHistory", "australia");
        Question WorldHistory800 = new Question("What was the first country Nazi Germany invaded during World War II?", 800, "WorldHistory", "poland");
        Question WorldHistory1200 = new Question ("Which country's genocide was depicted in a movie with Hotel in the title?", 1200, "WorldHistory", "rwanda");
        Question WorldHistory1600 = new Question ("What is the last name of the Italian dictator during World War I?", 1600, "WorldHistory", "mussolini");
        Question WorldHistory2000 = new Question ("This band's name is inspired by an Archduke who's death started World War I?", 2000, "WorldHistory", "franz ferdinand");

        ArrayList<Question> db = new ArrayList<Question>();
        db.add(Grammar200);
        db.add(Grammar400);
        db.add(Grammar600);
        db.add(Grammar800);
        db.add(Grammar1000);
        db.add(AmericanHistory200);
        db.add(AmericanHistory400);
        db.add(AmericanHistory600);
        db.add(AmericanHistory800);
        db.add(AmericanHistory1000);
        db.add(Anatomy200);
        db.add(Anatomy400);
        db.add(Anatomy600);
        db.add(Anatomy800);
        db.add(Anatomy1000);
        db.add(Animals200);
        db.add(Animals400);
        db.add(Animals600);
        db.add(Animals800);
        db.add(Animals1000);
        db.add(Geography200);
        db.add(Geography400);
        db.add(Geography600);
        db.add(Geography800);
        db.add(Geography1000);
        db.add(Literature200);
        db.add(Literature400);
        db.add(Literature600);
        db.add(Literature800);
        db.add(Literature1000);
        db.add(Presidents400);
        db.add(Presidents800);
        db.add(Presidents1200);
        db.add(Presidents1600);
        db.add(Presidents2000);
        db.add(Government400);
        db.add(Government800);
        db.add(Government1200);
        db.add(Government1600);
        db.add(Government2000);
        db.add(Chemistry400);
        db.add(Chemistry800);
        db.add(Chemistry1200);
        db.add(Chemistry1600);
        db.add(Chemistry2000);
        db.add(Physics400);
        db.add(Physics800);
        db.add(Physics1200);
        db.add(Physics1600);
        db.add(Physics2000);
        db.add(Psychology400);
        db.add(Psychology800);
        db.add(Psychology1200);
        db.add(Psychology1600);
        db.add(Psychology2000);
        db.add(WorldHistory400);
        db.add(WorldHistory800);
        db.add(WorldHistory1200);
        db.add(WorldHistory1600);
        db.add(WorldHistory2000);

        return db;




    }

}
