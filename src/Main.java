import com.sun.corba.se.impl.orb.DataCollectorBase;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        // can change category based on known category options in db
        String category = "Grammar";
        // can change pointvalue based on known pointvalue options in db
        Integer pointValue = 200;

        Question interfaceCat = new Question("", null, category, "");
        Question interfacePV = new Question("", pointValue, "", "");

        Database questionGrabber = new Database();
        Database answerGrabber = new Database();
        String questionReceiver = questionGrabber.selectQuestion(interfacePV, interfaceCat);
        String answerReceiver = answerGrabber.getAnswer(interfacePV, interfaceCat);

        System.out.println("for the given" + category + "and given pointValue" + pointValue.toString() + "the question is: " + questionReceiver);
        System.out.println("for the given" + category + "and given pointValue" + pointValue.toString() + "the answer is: " + answerReceiver);
    }

}


