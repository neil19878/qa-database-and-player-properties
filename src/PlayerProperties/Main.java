package PlayerProperties;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        PlayerProperties gamer = new PlayerProperties(0, 0, "");

        String gamerName = gamer.assignName("Andy Cameron");
        Integer addOne = gamer.updateTokenCount();
        Integer useOne = gamer.useToken();
        Integer addScore = gamer.updateScore(100);

        System.out.println("Gamer Stats: " + gamerName +"," + addOne + "," + useOne + "," + addScore + ".");
    }

}
