package PlayerProperties;

public class PlayerProperties {
    public Integer score;
    public Integer freeSpinTokenCount;
    public String playerName;

    public PlayerProperties() {
        score = 0;
        freeSpinTokenCount = 1;
        playerName = "";
    }

    public PlayerProperties(Integer score, Integer  fSTC, String pn){
        this.score = score;
        this.freeSpinTokenCount = fSTC;
        this.playerName = pn;
    }

    public Integer useToken(){
        freeSpinTokenCount -= 1;
        return freeSpinTokenCount;
    }

    public Integer updateScore(Integer scoreState){
        score += scoreState;

        return score;
    }

    public Integer updateTokenCount(){
        freeSpinTokenCount += 1;

        return freeSpinTokenCount;
    }

    public String assignName(String named) {
        playerName = named;

        return playerName;
    }

    public Integer getScore(){
        return score;
    }

    public Integer getFreeSpinTokenCount() {
        return freeSpinTokenCount;
    }

    public String getPlayerName(){
        return playerName;
    }

    public void setScore(Integer score){
        this.score = score;
    }

    public void setFreeSpinTokenCount(Integer freeSpinTokenCount){
        this.freeSpinTokenCount = freeSpinTokenCount;
    }

    public void setPlayerName(String playerName){
        this.playerName = playerName;
    }

    public String toString(){
        String output = "";
        output += "\t score: " + score + "\n";
        output += "\t free spin token count: " + freeSpinTokenCount + "\n";
        output += "\t player name: " + playerName + "\n";

        return output;
    }
}
