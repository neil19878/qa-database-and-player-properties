public class Question {
    //atributes
    public String question;
    public Integer pointValue;
    public String category;
    public String answer;


    public Question(){
        question = "";
        pointValue = null;
        category = "";
        answer = "";
    }

    public Question(String question, Integer pointValue, String category, String answer){
        this.question = question;
        this.pointValue = pointValue;
        this.category = category;
        this.answer = answer;
    }

    public Question(Integer pointValue){
        this.question = question;
        this.pointValue = pointValue;
        this.category = category;
        this.answer = answer;
    }

    public Question(String category){
        this.question = question;
        this.pointValue = pointValue;
        this.category = category;
        this.answer = answer;
    }


/*
    public String getQuestion(){
        return question;
    }

    public Integer getPointValue(){
        return pointValue;
    }

    public String getCategory(){
        return category;
    }

    public String getAnswer(){

        return answer;
    }

    public void setQuestion(String question){

        this.question = question;
    }

    public void setPointValue(Integer pointValue){

        this.pointValue = pointValue;
    }

    public void setCategory(String category){

        this.category = category;
    }

    public void setAnswer(String answer){

        this.answer = answer;
    }

    public String toString(){
        String output = "";
        output += "\t question: " + question + "\n";
        output += "\t point value: " + pointValue + "\n";
        output += "\t category: " + category + "\n";
        output += "\t answer: " + answer + "\n";

        return output;

    }

    */

}
